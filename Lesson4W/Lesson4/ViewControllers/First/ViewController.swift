
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    
    override func loadView() {
        super.loadView()
        print(#function)
    }
    
    override func loadViewIfNeeded() {
        super.loadViewIfNeeded()
        print(#function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
        
//        let uiview = UIView()
//        uiview.backgroundColor = .red
//        uiview.frame = CGRect(x: self.view.center.x  - 100, y: self.view.center.y - 100, width: 200, height: 200)
//        self.view.addSubview(uiview)
            
//        print(self.view.frame)
//        print(self.view.bounds)
//        self.view.isHidden
        
        
        //ДЗ
//        Реализовать гонку с прошлого домашнего задания на UI(User interface)
        
        let carImageViewsArray = [UIImageView(image: UIImage(named: "car")), UIImageView(image: UIImage(named: "car")), UIImageView(image: UIImage(named: "car"))]

        var timesInSeconds = 5
        var startCarsX = 20
        carImageViewsArray.forEach({ car in
            timesInSeconds += 5
            startCarsX += 72
            car.frame = CGRect(x: startCarsX, y: (Int(self.view.center.y) * 2) , width: 72, height: 72)
            self.view.addSubview(car)

           
            UIView.animate(withDuration: TimeInterval(timesInSeconds), animations: {
                car.frame = CGRect(x: startCarsX, y: 0 , width: 72, height: 72)
            })
        })
              
        
//        print("button bounds: ", nextButton.bounds)
//        print("button frame: ", nextButton.frame)
//        print("bounds self view: ", self.view.bounds)
//        print("frame self view: ", self.view.frame)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print(#function)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(#function)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print(#function)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(#function, "---------")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        print(#function)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(#function)
    }
    
    deinit {
        print(#function)
    }
    
    @IBAction func goToSecondScreenButtonAction(_ sender: UIButton) {
         let vc = UIStoryboard.init(name: "Second", bundle: nil).instantiateInitialViewController() as! SecondViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

