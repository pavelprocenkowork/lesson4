
import UIKit

class SecondViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
        print(#function)
    }
    
    override func loadViewIfNeeded() {
        super.loadViewIfNeeded()
        print(#function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print(#function)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(#function)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print(#function)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(#function, "---------")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        print(#function)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(#function)
    }
    
    deinit {
        print(#function)
    }
    
    @IBAction func backToFirstScreenAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
